execute "apt-get update" do
  command "apt-get update"
end

# OS Dendencies
%w(git libmysqlclient-dev).each do |pkg|
  package pkg
end


#
# Update dependencies once.
#
execute 'apt-get update' do
  ignore_failure true
end

#
# Install Ruby Build Dependencies
#
package 'libxslt-dev'
package 'libxml2-dev'
package 'build-essential'
package 'libpq-dev'
package 'libsqlite3-dev'

#
# Add apt-add-repository.
#
package 'software-properties-common'

#
# Add brightbox ruby repo.
#
execute 'apt-add-repository ppa:brightbox/ruby-ng -y' do
  not_if 'which ruby | grep -c 2.1'
end

#
# Update dependencies again.
#
execute 'apt-get update' do
  ignore_failure true
end

#
# Install Ruby 2.1
#
package 'ruby2.1'
package 'ruby2.1-dev'

#
# Install Bundler, build it against the newly installed 2.1 gem binary
#
gem_package 'bundler' do
  gem_binary('/usr/bin/gem2.1')
end

#
# Install yajl-ruby, required for re-provisioning Chef.
#
gem_package 'yajl-ruby' do
  gem_binary('/usr/bin/gem2.1')
end

#
# Install Rails.
#
gem_package 'rails' do
  gem_binary('/usr/bin/gem2.1')
  version '4.2.5'
end



execute 'apt-add-repository ppa:chris-lea/redis-server -y'

execute 'apt-get update' do
  ignore_failure true
end

package 'redis-server'

# Install NodeJS for Front Tail

include_recipe 'nodejs'
nodejs_npm 'connect'
nodejs_npm 'daemon'
nodejs_npm 'error'
nodejs_npm 'serve-static'
nodejs_npm 'socket.io'
nodejs_npm 'forever'


include_recipe "apache2::mod_ssl"
include_recipe "apache2::mod_proxy"
include_recipe "apache2::mod_proxy_http"
include_recipe "apache2::mod_proxy_balancer"
include_recipe "apache2::mod_rewrite"
include_recipe "apache2::mod_mime"
include_recipe "apache2::mod_expires"
include_recipe "apache2::mod_headers"
include_recipe "apache2::mod_ssl"
include_recipe "apache2::mod_proxy_wstunnel"
include_recipe "apache2::mod_auth_basic"
include_recipe "apache2::mod_socache_shmcb"
include_recipe "apache2::mod_reqtimeout"
include_recipe "apache2::mod_mime"
include_recipe "apache2::mod_filter"
include_recipe "apache2::mod_env"
include_recipe "apache2::mod_proxy_connect"
include_recipe "apache2::mod_dir"
include_recipe "apache2::mod_cache"
include_recipe "apache2::mod_auth_digest"


execute "copy_authorized_keys" do
  command "sudo cp /home/ubuntu/.ssh/authorized_keys /root/.ssh/"
end




