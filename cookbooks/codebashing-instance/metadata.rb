name             'codebashing-instance'
maintainer       'The Authors'
maintainer_email 'you@example.com'
license          'all_rights'
description      'Installs/Configures codebashing-instance'
long_description 'Installs/Configures codebashing-instance'
version          '0.1.0'

depends "apache"
depends "nodejs"
